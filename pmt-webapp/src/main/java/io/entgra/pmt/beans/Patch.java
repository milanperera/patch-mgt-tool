/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.beans;

public class Patch {

    private String patchId;
    private int productId;
    private IssueType issueType;
    private String customerId;
    private String publicIssue;
    private String privateIssue;
    private String reporter;
    private ETA eta;
    private String developer;
    private String publicPR;
    private String supportPR;
    private String issueDescription;
    private PatchArtifacts patchArtifacts;
    private PatchLifeCycleState patchLifeCycleState;

    public PatchLifeCycleState getPatchLifeCycleState() {
        return patchLifeCycleState;
    }

    public void setPatchLifeCycleState(PatchLifeCycleState patchLifeCycleState) {
        this.patchLifeCycleState = patchLifeCycleState;
    }

    public void setPatchLifeCycleState(String patchLifeCycleState) {
        if (patchLifeCycleState == null) {
            this.patchLifeCycleState = PatchLifeCycleState.ANY;
        } else {
            this.patchLifeCycleState = PatchLifeCycleState.valueOf(patchLifeCycleState);
        }
    }

    public String getIssueDescription() {
        return issueDescription;
    }

    public void setIssueDescription(String issueDescription) {
        this.issueDescription = issueDescription;
    }

    public String getPatchId() {
        return patchId;
    }

    public void setPatchId(String patchId) {
        this.patchId = patchId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public IssueType getIssueType() {
        return issueType;
    }

    public void setIssueType(IssueType issueType) {
        this.issueType = issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = IssueType.valueOf(issueType);
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPublicIssue() {
        return publicIssue;
    }

    public void setPublicIssue(String publicIssue) {
        this.publicIssue = publicIssue;
    }

    public String getPrivateIssue() {
        return privateIssue;
    }

    public void setPrivateIssue(String privateIssue) {
        this.privateIssue = privateIssue;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public ETA getEta() {
        return eta;
    }

    public void setEta(ETA eta) {
        this.eta = eta;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getPublicPR() {
        return publicPR;
    }

    public void setPublicPR(String publicPR) {
        this.publicPR = publicPR;
    }

    public String getSupportPR() {
        return supportPR;
    }

    public void setSupportPR(String supportPR) {
        this.supportPR = supportPR;
    }

    public PatchArtifacts getPatchArtifacts() {
        return patchArtifacts;
    }

    public void setPatchArtifacts(PatchArtifacts patchArtifacts) {
        this.patchArtifacts = patchArtifacts;
    }
}
