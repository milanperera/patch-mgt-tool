/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.beans;

import io.entgra.pmt.exception.ValidationException;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ETA {

    private Date bestCase;
    private Date mostLikely;
    private Date worstCase;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    public Date getBestCase() {
        return bestCase;
    }

    public void setBestCase(String bestCase) throws ValidationException {
        try {
            formatter.setLenient(false);
            this.bestCase = new java.sql.Date(formatter.parse(bestCase).getTime());
        } catch (ParseException e) {
            throw new ValidationException("Unable to parse the value given for 'bestCase'. Valid format yyyy-MM-dd" );
        }
    }
    public void setBestCase(Date bestCase) {
        this.bestCase = bestCase;
    }

    public Date getMostLikely() {
        return mostLikely;
    }

    public void setMostLikely(String mostLikely) throws ValidationException {
        try {
            formatter.setLenient(false);
            this.mostLikely = new Date(formatter.parse(mostLikely).getTime());
        } catch (ParseException e) {
            throw new ValidationException("Unable to parse the value given for 'mostLikely'. Valid format yyyy-MM-dd" );
        }
    }

    public void setMostLikely(Date mostLikely) {
        this.mostLikely = mostLikely;
    }

    public Date getWorstCase() {
        return worstCase;
    }

    public void setWorstCase(String worstCase) throws ValidationException {
        try {
            formatter.setLenient(false);
            this.worstCase = new Date(formatter.parse(worstCase).getTime());
        } catch (ParseException e) {
            throw new ValidationException("Unable to parse the value given for 'worstCase'. Valid format yyyy-MM-dd" );
        }
    }

    public void setWorstCase(Date worstCase) {
        this.worstCase = worstCase;
    }
}
