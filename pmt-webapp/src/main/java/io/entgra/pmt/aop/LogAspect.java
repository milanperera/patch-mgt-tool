/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.aop;

import io.entgra.pmt.beans.Response;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class LogAspect {

    private Logger logger = LoggerFactory.getLogger(LogAspect.class);

    @AfterReturning(value = "execution(* io.entgra.pmt.service.api.*.*(..))", returning = "result")
    public void afterReturn(JoinPoint joinPoint, Object result) {
        if (result instanceof Response) {
            logger.info("{} {}", joinPoint.toShortString(), ((Response) result).getMessage());
        } else {
            logger.info("{}", joinPoint.toShortString());
        }
    }

    @AfterThrowing(value = "execution(* io.entgra.pmt.service.api.*.*(..))", throwing = "e")
    public void afterThrow(JoinPoint joinPoint, Exception e) {
        logger.error(joinPoint.toShortString(), e);
    }

}
