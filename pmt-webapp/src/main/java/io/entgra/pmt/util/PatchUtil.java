/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.util;

import io.entgra.pmt.beans.Patch;
import io.entgra.pmt.beans.PatchArtifacts;
import io.entgra.pmt.exception.ValidationException;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PatchUtil {

    public static void validatePatchEntry(Patch patch) throws ValidationException {
        List<String> invalidatedFields = new ArrayList<>();

        if (StringUtils.isEmpty(patch.getCustomerId())) {
            invalidatedFields.add("customerId");
        }
        if (StringUtils.isEmpty(patch.getIssueType())) {
            invalidatedFields.add("issueType");
        }
        if (StringUtils.isEmpty(patch.getReporter())) {
            invalidatedFields.add("reporter");
        }
        if (StringUtils.isEmpty(patch.getPrivateIssue())) {
            invalidatedFields.add("privateIssue");
        }
        if (StringUtils.isEmpty(patch.getPublicIssue())) {
            invalidatedFields.add("publicIssue");
        }
        if (patch.getProductId() == 0) {
            invalidatedFields.add("productId");
        }
        if (!invalidatedFields.isEmpty()) {
            throw new ValidationException(invalidatedFields);
        }
    }

    public static void validateETA(Patch patch) throws ValidationException {
        List<String> invalidatedFields = new ArrayList<>();

        if (patch.getEta() == null) {
            invalidatedFields.add("No ETA object found");
            throw new ValidationException("No ETA object found");
        }
        if (StringUtils.isEmpty(patch.getEta().getBestCase())) {
            invalidatedFields.add("bestCase");
        }
        if (StringUtils.isEmpty(patch.getEta().getMostLikely())) {
            invalidatedFields.add("mostLikely");
        }
        if (StringUtils.isEmpty(patch.getEta().getWorstCase())) {
            invalidatedFields.add("worstCase");
        }
        if (StringUtils.isEmpty(patch.getDeveloper())) {
            invalidatedFields.add("developer");
        }
        if (!invalidatedFields.isEmpty()) {
            throw new ValidationException(invalidatedFields);
        }
    }

    public static void validateQA(Patch patch) throws ValidationException {
        List<String> invalidatedFields = new ArrayList<>();

        if (StringUtils.isEmpty(patch.getPublicPR())) {
            invalidatedFields.add("publicPR");
        }
        if (StringUtils.isEmpty(patch.getSupportPR())) {
            invalidatedFields.add("supportPR");
        }
        if (!invalidatedFields.isEmpty()) {
            throw new ValidationException(invalidatedFields);
        }
    }

    public static void validateDelivery(PatchArtifacts artifacts) throws ValidationException {
        if (artifacts.getJars().isEmpty()  && artifacts.getWebapps().isEmpty() && artifacts.getResources().isEmpty()) {
            throw new ValidationException("No artifacts object found");
        }
    }

}
