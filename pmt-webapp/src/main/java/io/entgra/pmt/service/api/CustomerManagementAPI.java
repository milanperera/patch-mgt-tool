/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.service.api;

import io.entgra.pmt.beans.Customer;
import io.entgra.pmt.beans.Response;
import io.entgra.pmt.dao.CustomerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:8000", "http://pmt.entgra.io", "http://support.entgra.io"})
@RestController
@RequestMapping("/customer")
public class CustomerManagementAPI {

    @Autowired
    private CustomerDAO customerDAO;

    @PostMapping("/")
    public Response addCustomer(@RequestBody Customer customer) {
        if (customerDAO.addCustomer(customer) > 0) {
            return new Response(customer.getCustomerName() + " is added");
        }
        return new Response("Failed to add customer '" + customer.getCustomerName() + "'");
    }

    @PutMapping("/")
    public Response updateCustomer(@RequestBody Customer customer) {
        if (customerDAO.updateCustomer(customer) > 0) {
            return new Response(customer.getCustomerId() + " is updated");
        }
        return new Response("Failed to update customer '" + customer.getCustomerName() + "'");
    }

    @GetMapping("/")
    public List<Customer> listCustomers() {
        return customerDAO.getAllCustomers();
    }

    @GetMapping("/{id}")
    public Customer getCustomer(@PathVariable("id") String id) {
        return customerDAO.getCustomer(id);
    }

    @DeleteMapping("/{id}")
    public Response removeCustomer(@PathVariable("id") String id) {
        if (customerDAO.removeCustomer(id) > 0) {
            return new Response(id + " is removed");
        }
        return new Response("Failed to remove customer '" + id + "'");
    }

}
