/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.service.api;

import io.entgra.pmt.beans.*;
import io.entgra.pmt.exception.PMTException;
import io.entgra.pmt.service.PatchManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:8000", "http://pmt.entgra.io", "http://support.entgra.io"})
@RestController
@RequestMapping("/patch")
public class PatchManagementAPI {

    @Autowired
    private PatchManagementService patchManagementService;

    @PostMapping("/queue/add")
    public Response addPatchToQueue(@RequestBody Patch patch) throws PMTException {
        String patchId = patchManagementService.addPatchToQueue(patch);
        return new Response("Patch '" + patchId + "' is created");
    }

    @GetMapping("/queue")
    public PaginatedResult<Patch> getQueuedPatches(@RequestParam int pageNo) {
        return patchManagementService.getQueuedPatches(pageNo);
    }

    @GetMapping("/eta")
    public PaginatedResult<Patch> getETAPatches(@RequestParam int pageNo) {
        return patchManagementService.getETAPatches(pageNo);
    }

    @GetMapping("/deliver")
    public PaginatedResult<Patch> getDeliveredPatches(@RequestParam int pageNo) {
        return patchManagementService.getDeliveredPatches(pageNo);
    }

    @DeleteMapping("/{patchId}")
    public Response removeQueue(@PathVariable("patchId") String patchId) throws PMTException {
        patchManagementService.removePatch(patchId);
        return new Response("Patch '" + patchId + "' is removed");
    }

    @PutMapping("/eta/{patchId}")
    public Response updateETA(@PathVariable("patchId") String patchId, @RequestBody Patch patch) throws PMTException {
        patch.setPatchId(patchId);
        patchManagementService.updateETA(patch);
        return new Response("ETA of patch '" + patchId + "' is updated");
    }

    @PutMapping("/qa/{patchId}")
    public Response promotePatchToQA(@PathVariable("patchId") String patchId, @RequestBody Patch patch) throws PMTException {
        patch.setPatchId(patchId);
        patchManagementService.promoteToQA(patch);
        return new Response("QA details of patch '" + patchId + "' is updated");
    }

    @PutMapping("/deliver/{patchId}")
    public Response promotePatchToDeliver(@PathVariable("patchId") String patchId, @RequestBody PatchArtifacts artifacts)
            throws PMTException {
        patchManagementService.promoteToDeliver(patchId, artifacts);
        return new Response("Patch '" + patchId + "' is promoted to 'Delivered' state");
    }

    @GetMapping("")
    public PaginatedResult<Patch> getAllPatches(@RequestParam int pageNo) {
        return patchManagementService.getAllPatches(pageNo);
    }

    @GetMapping("/{patchId}")
    public Patch getPatch(@PathVariable("patchId") String patchId) throws PMTException {
        return patchManagementService.getPatch(patchId);
    }

    @GetMapping("/generate-id")
    public PatchID generatePatchId() {
        return new PatchID(patchManagementService.generatePatchNo());
    }

    @GetMapping("/stats")
    public List<Stat> getStats() throws PMTException {
        return patchManagementService.getStats();
    }

}
