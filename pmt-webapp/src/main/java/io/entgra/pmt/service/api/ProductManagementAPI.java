/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *  
 */

package io.entgra.pmt.service.api;

import io.entgra.pmt.beans.Product;
import io.entgra.pmt.beans.Response;
import io.entgra.pmt.dao.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:8000", "http://pmt.entgra.io", "http://support.entgra.io"})
@RestController
@RequestMapping("/product")
public class ProductManagementAPI {

    @Autowired
    private ProductDAO productDAO;

    @PostMapping("/")
    public Response addProduct(@RequestBody Product product) {
        if (productDAO.addProduct(product) > 0) {
            return new Response(product.getProductName() + " is added");
        }
        return new Response("Failed to add product '" + product.getProductName() + "-" + product.getProductVersion()+ "'");
    }

    @PutMapping("/")
    public Response updateProduct(@RequestBody Product product) {
        if (productDAO.updateProduct(product) > 0) {
            return new Response(product.getProductName() + " is updated");
        }
        return new Response("Failed to update product '" + product.getProductName() + "-" + product.getProductVersion()+ "'");
    }

    @GetMapping("/")
    public List<Product> listProducts() {
        return productDAO.getAllProducts();
    }

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable("id") int id) {
        return productDAO.getProduct(id);
    }

    @DeleteMapping("/{id}")
    public Response removeProduct(@PathVariable("id") int id) {
        if (productDAO.removeProduct(id) > 0) {
            return new Response("product id '" + id + "' is removed");
        }
        return new Response("Failed to remove product id '" + id + "'");
    }

}
