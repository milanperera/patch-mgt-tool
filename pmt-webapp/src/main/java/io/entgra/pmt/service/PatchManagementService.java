/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.service;

import io.entgra.pmt.beans.Stat;
import io.entgra.pmt.exception.DataPersistanceException;
import io.entgra.pmt.exception.ValidationException;
import io.entgra.pmt.util.PatchUtil;
import io.entgra.pmt.beans.PaginatedResult;
import io.entgra.pmt.beans.Patch;
import io.entgra.pmt.beans.PatchArtifacts;
import io.entgra.pmt.beans.PatchLifeCycleState;
import io.entgra.pmt.dao.PatchDAO;
import io.entgra.pmt.exception.IllegalLifeCycleStateChangeException;
import io.entgra.pmt.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class PatchManagementService {

    private Logger logger = LoggerFactory.getLogger(PatchManagementService.class);

    @Value("${io.entgra.pmt.patch.prefix}")
    private String PATCH_PREFIX;

    @Value("${io.entgra.pmt.page.size}")
    private int pageSize;

    @Autowired
    PatchDAO patchDAO;

    public String addPatchToQueue(Patch patch) throws ValidationException, DataPersistanceException {
        patch.setPatchId(generatePatchNo());
        patch.setPatchLifeCycleState(PatchLifeCycleState.QUEUED);
        PatchUtil.validatePatchEntry(patch);

        if (patchDAO.addToQueue(patch) > 0) {
            return patch.getPatchId();
        }
        throw new DataPersistanceException("Unable to add patch to the queue");
    }

    public String generatePatchNo() {
        String latestId = patchDAO.getLatestId();
        if (!StringUtils.isEmpty(latestId)) {
            int lastPatchNo = Integer.parseInt(latestId.substring(latestId.length() - 4));
            if (logger.isDebugEnabled()) {
                logger.debug("Last updated patch no: " + lastPatchNo);
            }
            return new StringBuilder().append(PATCH_PREFIX).append(lastPatchNo + 1).toString();
        } else {
            logger.warn("Latest patch Id cannot be retrieved");
        }
        return null;
    }

    public void removePatch(String patchId) throws DataPersistanceException, ResourceNotFoundException,
                                                   IllegalLifeCycleStateChangeException {
        validatePatch(patchId, PatchLifeCycleState.QUEUED, PatchLifeCycleState.REMOVED);
        if (patchDAO.removePatch(patchId) < 0) {
            throw new DataPersistanceException("Unable to remove patch '" + patchId + "'");
        }
    }

    public void updateETA(Patch patch) throws DataPersistanceException, ValidationException, ResourceNotFoundException,
                                              IllegalLifeCycleStateChangeException {
        validatePatch(patch.getPatchId(), PatchLifeCycleState.QUEUED, PatchLifeCycleState.DEVELOPMENT);
        PatchUtil.validateETA(patch);
        patch.setPatchLifeCycleState(PatchLifeCycleState.DEVELOPMENT);
        if (patchDAO.addETA(patch) < 0) {
            throw new DataPersistanceException("Unable to update ETA of patch '" + patch.getPatchId() + "'");
        }
    }

    public void promoteToQA(Patch patch) throws ValidationException, DataPersistanceException,
                                                ResourceNotFoundException, IllegalLifeCycleStateChangeException {
        validatePatch(patch.getPatchId(), PatchLifeCycleState.DEVELOPMENT, PatchLifeCycleState.QA);
        PatchUtil.validateQA(patch);
        patch.setPatchLifeCycleState(PatchLifeCycleState.QA);
        if (patchDAO.promoteToQA(patch) < 0) {
            throw new DataPersistanceException("Unable to update QA details of patch '" + patch.getPatchId() + "'");
        }
    }

    public void promoteToDeliver(String patchId, PatchArtifacts artifacts) throws ResourceNotFoundException,
                                                                                  DataPersistanceException,
                                                                                  IllegalLifeCycleStateChangeException,
                                                                                  ValidationException {
        PatchUtil.validateDelivery(artifacts);
        validatePatch(patchId, PatchLifeCycleState.QA, PatchLifeCycleState.DELIVERED);
        if (patchDAO.promoteToDeliver(patchId, artifacts) < 0) {
            throw new DataPersistanceException("Unable to promote patch '" + patchId + "'");
        }
    }

    public PaginatedResult<Patch> getQueuedPatches(int pageNo) {
        return patchDAO.getAllPatchesFromState(pageNo, pageSize, PatchLifeCycleState.QUEUED);
    }

    public PaginatedResult<Patch> getETAPatches(int pageNo) {
        return patchDAO.getAllPatchesFromState(pageNo, pageSize, PatchLifeCycleState.DEVELOPMENT);
    }

    public PaginatedResult<Patch> getDeliveredPatches(int pageNo) {
        return patchDAO.getAllPatchesFromState(pageNo, pageSize, PatchLifeCycleState.DELIVERED);
    }

    public PaginatedResult<Patch> getAllPatches(int pageNo) {
        return patchDAO.getAllPatches(pageNo, pageSize);
    }

    public Patch getPatch(String patchId) throws ResourceNotFoundException, DataPersistanceException {
        Patch patch = patchDAO.getPatch(patchId);
        if (patch == null) {
            throw new DataPersistanceException("Unable to retrieve data of patch '" + patchId + "'");
        }
        return patch;
    }

    public List<Stat> getStats() throws ResourceNotFoundException {
        return patchDAO.getStats();
    }

    private void validatePatch(String patchId, PatchLifeCycleState desiredState, PatchLifeCycleState nextState)
            throws ResourceNotFoundException, IllegalLifeCycleStateChangeException {
        String actualState = patchDAO.validatePatch(patchId);
        if (!StringUtils.isEmpty(actualState)) {
            if (actualState.equals(nextState.toString())) {
                return;
            }
            if (!desiredState.toString().equals(actualState)) {
                throw new IllegalLifeCycleStateChangeException("Cannot promote '" + actualState +
                                                               "' state to '" + nextState.toString() + "' state");
            }
        }
    }

}
