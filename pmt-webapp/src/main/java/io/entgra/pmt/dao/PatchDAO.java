/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.dao;

import io.entgra.pmt.beans.Patch;
import io.entgra.pmt.beans.ArtifactType;
import io.entgra.pmt.beans.ETA;
import io.entgra.pmt.beans.PaginatedResult;
import io.entgra.pmt.beans.PatchArtifacts;
import io.entgra.pmt.beans.PatchLifeCycleState;
import io.entgra.pmt.beans.Stat;
import io.entgra.pmt.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PatchDAO {

    private Logger logger = LoggerFactory.getLogger(PatchDAO.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // table column names
    private String PATCH_ID = "patch_id";
    private String PRODUCT_NAME = "product_id";
    private String ISSUE_TYPE = "issue_type";
    private String CUSTOMER_ID = "customer_id";
    private String PUBLIC_ISSUE = "public_issue";
    private String PRIVATE_ISSUE = "private_issue";
    private String ISSUE_DESC = "issue_description";
    private String REPORTER = "reporter";
    private String ETA_BEST_CASE = "eta_best_case";
    private String ETA_MOST_LIKELY = "eta_most_likely";
    private String ETA_WORST_CASE = "eta_worst_case";
    private String DEVELOPER = "developer";
    private String PUBLIC_PR = "public_pr";
    private String SUPPORT_PR = "support_pr";
    private String CURRENT_LC_STATE = "current_state";
    private String CREATED_TS = "created_time";
    private String LAST_UPDATED_TS = "last_updated_time";

    public String getLatestId() {
        String patchId = jdbcTemplate.queryForObject("SELECT patch_id FROM patch ORDER BY patch_id DESC LIMIT 1",
                                                     null,
                                                     (rs, rowNum) -> rs.getString(PATCH_ID));
        return patchId;
    }

    public int addPatch(Patch patch) {
        try {
            return jdbcTemplate.update("INSERT INTO patch (patch_id, product_id, issue_type, customer_id, " +
                                       "public_issue, private_issue, reporter, eta_best_case, eta_most_likely, " +
                                       "eta_worst_case, developer, public_pr, support_pr, issue_description) " +
                                       "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                       patch.getPatchId(), patch.getProductId(), patch.getIssueType(),
                                       patch.getCustomerId(), patch.getPublicIssue(), patch.getPrivateIssue(),
                                       patch.getReporter(), patch.getEta().getBestCase(), patch.getEta().getMostLikely(),
                                       patch.getEta().getWorstCase(), patch.getDeveloper(), patch.getPublicPR(),
                                       patch.getSupportPR(), patch.getIssueDescription());

        } catch (DataAccessException e) {
            logger.error("Unable to add the patch", e);
        }
        return -1;
    }

    public int addToQueue(Patch patch) {
        try {
            return jdbcTemplate.update("INSERT INTO patch (patch_id, product_id, issue_type, customer_id, " +
                                       "public_issue, private_issue, reporter, issue_description, current_state, " +
                                       "created_time, last_updated_time) " +
                                       "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                       patch.getPatchId(), patch.getProductId(), patch.getIssueType().toString(),
                                       patch.getCustomerId(), patch.getPublicIssue(), patch.getPrivateIssue(),
                                       patch.getReporter(), patch.getIssueDescription(),
                                       patch.getPatchLifeCycleState().toString(), getTime(), getTime());

        } catch (DataAccessException e) {
            logger.error("Unable to add the patch '" + patch.getPatchId() + "' to queue", e);
        }
        return -1;
    }

    public int removePatch(String patchId) {
        try {
            return jdbcTemplate.update("DELETE FROM patch WHERE patch_id = ?", patchId);
        } catch (DataAccessException e) {
            logger.error("Unable to remove patch '" + patchId + "' from queue", e);
        }
        return -1;
    }

    public int addETA(Patch patch) {
        StringBuilder sql = new StringBuilder("UPDATE patch SET ")
                .append(ETA_BEST_CASE).append("=?, ").
                append(ETA_MOST_LIKELY).append("=?, ").append(ETA_WORST_CASE).append("=?, ").append(DEVELOPER).
                append("=?, ").append(CURRENT_LC_STATE).append("=?, ").append(LAST_UPDATED_TS).append("=? ").
                        append("WHERE patch_id = ?");

        try {
            return jdbcTemplate.update(sql.toString(), patch.getEta().getBestCase(), patch.getEta().getMostLikely(),
                                       patch.getEta().getWorstCase(), patch.getDeveloper(),
                                       patch.getPatchLifeCycleState().toString(), getTime(), patch.getPatchId());
        } catch (DataAccessException e) {
            logger.error("Unable to update ETA of patch '" + patch.getPatchId() + "'", e);
        }
        return -1;
    }

    public int promoteToQA(Patch patch) {
        StringBuilder sql = new StringBuilder("UPDATE patch SET ").append(PUBLIC_PR ).append("=?, ").append(SUPPORT_PR).
                append("=?, ").append(CURRENT_LC_STATE).append("=?, ").append(LAST_UPDATED_TS).append("=? ").
                append("WHERE patch_id = ?");

        try {
            return jdbcTemplate.update(sql.toString(), patch.getPublicPR(), patch.getSupportPR(),
                                       patch.getPatchLifeCycleState().toString(), getTime(), patch.getPatchId());
        } catch (DataAccessException e) {
            logger.error("Unable to update QA details of patch '" + patch.getPatchId() + "'", e);
        }
        return -1;
    }

    public int promoteToDeliver(String patchId, PatchArtifacts artifacts) {
        StringBuilder sql = new StringBuilder("UPDATE patch SET ").append(CURRENT_LC_STATE).append("=?, ").
                append(LAST_UPDATED_TS).append("=? ").append("WHERE patch_id = ?");

        try {
            if (addArtifacts(patchId, artifacts) != null) {
                return jdbcTemplate.update(sql.toString(), PatchLifeCycleState.DELIVERED.toString(), getTime(), patchId);
            }
        } catch (DataAccessException e) {
            logger.error("Unable to promote patch '" + patchId + "'", e);
        }
        return -1;
    }

    private int[] addArtifacts(String patchId, PatchArtifacts artifacts) {
        List<Object[]> params = new ArrayList<>();
        Object[] paramArtifacts;
        for (String jar : artifacts.getJars()) {
            paramArtifacts = new Object[] {patchId, ArtifactType.JAR.toString(), jar};
            params.add(paramArtifacts);
        }
        for (String war : artifacts.getWebapps()) {
            paramArtifacts = new Object[] {patchId, ArtifactType.WAR.toString(), war};
            params.add(paramArtifacts);
        }
        for (String resource : artifacts.getResources()) {
            paramArtifacts = new Object[] {patchId, ArtifactType.RESOURCE.toString(), resource};
            params.add(paramArtifacts);
        }
        String sql = "INSERT INTO artifact (patch_id, type, file) VALUES (?, ?, ?)";
        try {
            return jdbcTemplate.batchUpdate(sql, params);
        } catch (DataAccessException e) {
            logger.error("Unable to add artifacts of patch '" + patchId + "': " + e.getMessage());
            return null;
        }
    }

    public List<Stat> getStats() throws ResourceNotFoundException {
        try {
            List<Stat> stats = jdbcTemplate.query(
                    "SELECT current_state, count(current_state) as count FROM PMT.patch group by current_state;",
                    (rs, rowNum) -> {
                        Stat s = new Stat();
                        s.setLifeCycleState(rs.getString(CURRENT_LC_STATE));
                        s.setCount(rs.getInt("count"));
                        return s;
                    }

            );
            return stats;
        } catch (EmptyResultDataAccessException e) {
            logger.error("Unable to retrieve stats");
            throw new ResourceNotFoundException("Unable to retrieve stats", e);
        } catch (DataAccessException e) {
            logger.error("Unable to retrieve stats", e);
            return null;
        }
    }

    public String validatePatch(String patchId) throws ResourceNotFoundException {
        try {
            return jdbcTemplate.queryForObject("SELECT current_state FROM patch WHERE patch_id = ?", new Object[]{patchId},
                                               (rs, rowNum) -> rs.getString(CURRENT_LC_STATE));
        } catch (EmptyResultDataAccessException e) {
            logger.error("Unable to find patch '" + patchId + "'");
            throw new ResourceNotFoundException("Unable to find patch '" + patchId + "'", e);
        } catch (DataAccessException e) {
            logger.error("Unable to find patch '" + patchId + "'", e);
            return null;
        }
    }

    public PaginatedResult<Patch> getAllPatchesFromState(int pageNo, int pageSize, PatchLifeCycleState lifeCycleState) {
        int pageOffset = pageNo * pageSize;

        try {
            int total = getTotal(lifeCycleState);

            StringBuilder querySql = new StringBuilder("SELECT ");
            querySql.append(PATCH_ID).append(", ");
            querySql.append(PRODUCT_NAME).append(", ");
            querySql.append(ISSUE_TYPE).append(", ");
            querySql.append(CUSTOMER_ID).append(", ");
            querySql.append(PUBLIC_ISSUE).append(", ");
            querySql.append(PRIVATE_ISSUE).append(", ");
            querySql.append(ISSUE_DESC).append(", ");
            querySql.append(REPORTER).append(", ");
            querySql.append(ETA_BEST_CASE).append(", ");
            querySql.append(ETA_MOST_LIKELY).append(", ");
            querySql.append(ETA_WORST_CASE).append(", ");
            querySql.append(CURRENT_LC_STATE).append(" ");
            querySql.append("FROM patch ");
            querySql.append("WHERE ").append(CURRENT_LC_STATE).append(" = ?");
            querySql.append(" ORDER BY ").append(PATCH_ID).append(" DESC ").append("LIMIT ").append(pageSize).append(" ");
            querySql.append("OFFSET ").append(pageOffset);

            List<Patch> patches = jdbcTemplate.query(
                    querySql.toString(),
                    new Object[] {lifeCycleState.toString()},
                    (rs, rowNum) -> {
                        Patch patch = new Patch();
                        patch.setPatchId(rs.getString(PATCH_ID));
                        patch.setProductId(rs.getInt(PRODUCT_NAME));
                        patch.setIssueType(rs.getString(ISSUE_TYPE));
                        patch.setCustomerId(rs.getString(CUSTOMER_ID));
                        patch.setPublicIssue(rs.getString(PUBLIC_ISSUE));
                        patch.setPrivateIssue(rs.getString(PRIVATE_ISSUE));
                        patch.setIssueDescription(rs.getString(ISSUE_DESC));
                        patch.setReporter(rs.getString(REPORTER));
                        patch.setPatchLifeCycleState(rs.getString(CURRENT_LC_STATE));
                        ETA eta = new ETA();
                        eta.setBestCase(rs.getDate(ETA_BEST_CASE));
                        eta.setMostLikely(rs.getDate(ETA_MOST_LIKELY));
                        eta.setWorstCase(rs.getDate(ETA_WORST_CASE));
                        patch.setEta(eta);
                        return patch;
                    }

            );
            PaginatedResult<Patch> result = new PaginatedResult<>();
            result.setPageNo(pageNo);
            result.setPageSize(pageSize);
            result.setTotal(total);
            result.setResults(patches);
            result.setTotalPages(pageSize, total);
            return result;

        } catch (DataAccessException e) {
            logger.error("Unable to retrieve patches from patch queue", e);
        }
        return null;
    }

    public PaginatedResult<Patch> getAllPatches(int pageNo, int pageSize) {
        int pageOffset = pageNo * pageSize;

        try {
            int total = getTotal(PatchLifeCycleState.ANY);

            StringBuilder querySql = new StringBuilder("SELECT * ");
            querySql.append("FROM patch ");
            querySql.append("ORDER BY ").append(PATCH_ID).append(" ");
            querySql.append("LIMIT ").append(pageSize).append(" ");
            querySql.append("OFFSET ").append(pageOffset);

            List<Patch> patches = jdbcTemplate.query(
                    querySql.toString(),
                    (rs, rowNum) -> {
                        try {
                            return extractPatch(rs);
                        } catch (ResourceNotFoundException e) {
                            logger.error("Unable to retrieve patch artifacts of " + rs.getString(PATCH_ID), e);
                            return null;
                        }
                    });

            PaginatedResult<Patch> result = new PaginatedResult<>();
            result.setPageNo(pageNo);
            result.setPageSize(pageSize);
            result.setTotal(total);
            result.setResults(patches);
            result.setTotalPages(pageSize, total);
            return result;

        } catch (DataAccessException e) {
            logger.error("Unable to retrieve patches", e);
        }
        return null;
    }

    public Patch getPatch(String patchId) throws ResourceNotFoundException {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM patch WHERE patch_id = ?",
                                                  new Object[] {patchId},
                                                  (rs, rowNum) -> {
                                                      try {
                                                          return extractPatch(rs);
                                                      } catch (ResourceNotFoundException e) {
                                                          logger.error("Unable to retrieve patch artifacts of " + patchId, e);
                                                          return null;
                                                      }
                                                  });
        } catch (EmptyResultDataAccessException e) {
            logger.error("Unable to find patch '" + patchId + "'");
            throw new ResourceNotFoundException("Unable to find patch '" + patchId + "'", e);
        } catch (DataAccessException e) {
            logger.error("Unable to find patch '" + patchId + "'", e);
            return null;
        }
    }

    private PatchArtifacts getPatchArtifacts(String patchId) throws ResourceNotFoundException {
        PatchArtifacts patchArtifacts = new PatchArtifacts();
        try {
            jdbcTemplate.query("SELECT * FROM artifact WHERE patch_id = ?", new Object[]{patchId},
                                        (rs, rowNum) -> {
                                            switch (rs.getString("type")) {
                                                case "JAR" :
                                                    patchArtifacts.getJars().add(rs.getString("file"));
                                                    break;
                                                case "WAR" :
                                                    patchArtifacts.getWebapps().add(rs.getString("file"));
                                                    break;
                                                default:
                                                    patchArtifacts.getResources().add(rs.getString("file"));
                                            }
                                            return null;
                                        });
            return patchArtifacts;
        } catch (EmptyResultDataAccessException e) {
            logger.error("Unable to find patch '" + patchId + "'");
            throw new ResourceNotFoundException("Unable to find patch '" + patchId + "'", e);
        } catch (DataAccessException e) {
            logger.error("Unable to find patch '" + patchId + "'", e);
            return null;
        }
    }

    private int getTotal(PatchLifeCycleState state) {

        StringBuilder rowCountSql = new StringBuilder("SELECT count(1) AS row_count FROM patch");
        Object params[] = null;
        if (!"ANY".equals(state.toString())) {
            rowCountSql.append(" WHERE ").append(CURRENT_LC_STATE).append(" = ?");
            params = new Object[] {state.toString()};
        }
        return jdbcTemplate.queryForObject(
                rowCountSql.toString(),
                params, (rs, rowNum) -> rs.getInt(1)
        );
    }

    private Patch extractPatch(ResultSet rs) throws SQLException, ResourceNotFoundException {
        Patch patch = new Patch();
        patch.setPatchId(rs.getString(PATCH_ID));
        patch.setProductId(rs.getInt(PRODUCT_NAME));
        patch.setIssueType(rs.getString(ISSUE_TYPE));
        patch.setCustomerId(rs.getString(CUSTOMER_ID));
        patch.setPublicIssue(rs.getString(PUBLIC_ISSUE));
        patch.setPrivateIssue(rs.getString(PRIVATE_ISSUE));
        patch.setIssueDescription(rs.getString(ISSUE_DESC));
        patch.setReporter(rs.getString(REPORTER));
        patch.setPatchLifeCycleState(rs.getString(CURRENT_LC_STATE));
        ETA eta = new ETA();
        eta.setBestCase(rs.getDate(ETA_BEST_CASE));
        eta.setMostLikely(rs.getDate(ETA_MOST_LIKELY));
        eta.setWorstCase(rs.getDate(ETA_WORST_CASE));
        patch.setEta(eta);
        patch.setDeveloper(rs.getString(DEVELOPER));
        patch.setPublicPR(rs.getString(PUBLIC_PR));
        patch.setSupportPR(rs.getString(SUPPORT_PR));
        patch.setPatchArtifacts(getPatchArtifacts(patch.getPatchId()));
        return patch;
    }

    private long getTime() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp.getTime();
    }

}
