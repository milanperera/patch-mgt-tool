/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.dao;

import io.entgra.pmt.beans.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDAO {

    private Logger logger = LoggerFactory.getLogger(CustomerDAO.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Customer getCustomer(String customerId) {
        try {
            Customer customer = jdbcTemplate.queryForObject(
                    "SELECT name FROM customer WHERE id = ? ",
                    new Object[] {customerId}, (rs, rowNum) -> {
                        Customer c = new Customer();
                        c.setCustomerId(customerId);
                        c.setCustomerName(rs.getString("name"));
                        return c;
                    }
            );

            return customer;
        } catch (DataAccessException e) {
            logger.error("Unable to get customer info of '" + customerId + "'", e);
        }
        return null;
    }

    public List<Customer> getAllCustomers() {
        try {
            List<Customer> result = jdbcTemplate.query(
                    "SELECT id, name, created_date FROM customer",
                    (rs, rawNo) -> new Customer(rs.getString("id"), rs.getString("name"))
            );
            return result;
        } catch (DataAccessException e) {
            logger.error("Unable to get all customer info", e);
        }
        return null;
    }

    public int addCustomer(Customer customer) {
        try {
            return jdbcTemplate.update("INSERT INTO customer (id, name) VALUES (?, ?)",
                                       customer.getCustomerId(), customer.getCustomerName());
        } catch (DataAccessException e) {
            logger.error("Unable to add the customer", e);
        }
        return -1;
    }

    public int updateCustomer(Customer customer) {
        try {
            return jdbcTemplate.update("UPDATE customer SET name = ? WHERE id = ?",
                                customer.getCustomerName(), customer.getCustomerId());
        } catch (DataAccessException e) {
            logger.error("Unable to update '" + customer.getCustomerName() + "'", e);
        }
        return -1;
    }

    public int removeCustomer(String customerId) {
        try {
            return jdbcTemplate.update("DELETE FROM customer WHERE id = ?", customerId);
        } catch (DataAccessException e) {
            logger.error("Unable to remove '" + customerId + "'", e);
        }
        return -1;
    }

}
