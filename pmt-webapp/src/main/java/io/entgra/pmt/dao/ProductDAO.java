/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

package io.entgra.pmt.dao;

import io.entgra.pmt.beans.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class represents the DAO operations.
 */
@Repository
public class ProductDAO {

    private Logger logger = LoggerFactory.getLogger(ProductDAO.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * @param productId
     * @return Returns Product instance
     */
    public Product getProduct(int productId) {
        try {
            Product product = jdbcTemplate.queryForObject(
                    "SELECT * FROM product WHERE id = ? ",
                    new Object[] {productId}, (rs, rowNum) -> {
                        Product p = new Product();
                        p.setProductId(productId);
                        p.setCarbonVersion(rs.getString("carbon_version"));
                        p.setProductName(rs.getString("product_name"));
                        p.setProductVersion(rs.getString("product_version"));
                        return p;
                    }
            );

            return product;
        } catch (DataAccessException e) {
            logger.error("Unable to get product info of '" + productId + "'", e);
        }
        return null;
    }

    public List<Product> getAllProducts() {
        try {
            List<Product> result = jdbcTemplate.query(
                    "SELECT * FROM product",
                    (rs, rawNo) -> {
                        Product p = new Product();
                        p.setProductId(rs.getInt("id"));
                        p.setCarbonVersion(rs.getString("carbon_version"));
                        p.setProductName(rs.getString("product_name"));
                        p.setProductVersion(rs.getString("product_version"));
                        return p;
                    }
            );
            return result;
        } catch (DataAccessException e) {
            logger.error("Unable to get all product info", e);
        }
        return null;
    }

    public int addProduct(Product product) {
        try {
            return jdbcTemplate.update("INSERT INTO product (product_name, product_version, carbon_version) VALUES (?, ?, ?)",
                                       product.getProductName(), product.getProductVersion(), product.getCarbonVersion());
        } catch (DataAccessException e) {
            logger.error("Unable to add the product", e);
        }
        return -1;
    }

    public int updateProduct(Product product) {
        try {
            return jdbcTemplate.update("UPDATE product SET product_name = ?, product_version = ?, carbon_version = ? WHERE id = ?",
                                       product.getProductName(), product.getProductVersion(), product.getCarbonVersion(), product.getProductId());
        } catch (DataAccessException e) {
            logger.error("Unable to update '" + product.getProductName() + "'", e);
        }
        return -1;
    }

    public int removeProduct(int productId) {
        try {
            return jdbcTemplate.update("DELETE FROM product WHERE id = ?", productId);
        } catch (DataAccessException e) {
            logger.error("Unable to remove product id '" + productId + "'", e);
        }
        return -1;
    }

}
