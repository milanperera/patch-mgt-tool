-- MySQL dump 10.13  Distrib 5.6.39, for macos10.13 (x86_64)
--
-- Host: localhost    Database: PMT
-- ------------------------------------------------------
-- Server version	5.6.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artifact`
--

DROP TABLE IF EXISTS `artifact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artifact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patch_id` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `file` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artifact`
--

LOCK TABLES `artifact` WRITE;
/*!40000 ALTER TABLE `artifact` DISABLE KEYS */;
INSERT INTO `artifact` VALUES (1,'ENTGRA-PATCH-7001','JAR','andes_3.2.50.jar'),(2,'ENTGRA-PATCH-7001','JAR','axiom_1.2.11.wso2v11.jar'),(3,'ENTGRA-PATCH-7001','JAR','axis2_1.6.1.wso2v23.jar'),(4,'ENTGRA-PATCH-7001','JAR','bcpkix-jdk15on_1.52.0.wso2v2.jar'),(5,'ENTGRA-PATCH-7001','JAR','bcprov-jdk15on_1.52.0.wso2v1.jar'),(6,'ENTGRA-PATCH-7001','JAR','commons-fileupload_1.3.2.wso2v1.jar'),(7,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.core_4.4.26.jar'),(8,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.event.processor.ui_2.1.23.jar'),(9,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.identity.oauth_5.6.63.jar'),(10,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.ndatasource.ui_4.6.21.jar'),(11,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.tryit_4.6.21.jar'),(12,'ENTGRA-PATCH-7001','JAR','synapse-commons_2.1.7.wso2v48.jar'),(13,'ENTGRA-PATCH-7001','JAR','synapse-core_2.1.7.wso2v48.jar'),(14,'ENTGRA-PATCH-7001','JAR','synapse-nhttp-transport_2.1.7.wso2v48.jar'),(15,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.appmgt.mdm.osgiconnector_4.1.20.jar'),(16,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.identity.application.authentication.framework_5.11.148.jar'),(17,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.identity.application.common_5.11.148.jar'),(18,'ENTGRA-PATCH-7001','JAR','org.wso2.carbon.tenant.keystore.mgt_4.6.11.jar'),(19,'ENTGRA-PATCH-7002','JAR','NA'),(20,'ENTGRA-PATCH-7002','WAR','NA'),(21,'ENTGRA-PATCH-7002','RESOURCE','NA'),(22,'ENTGRA-PATCH-7013','JAR','NA'),(23,'ENTGRA-PATCH-7013','WAR','api#device-mgt#android#v1.0.war'),(24,'ENTGRA-PATCH-7013','RESOURCE','NA'),(25,'ENTGRA-PATCH-7012','JAR','org.wso2.carbon.device.mgt.ios.payload-3.4.0.jar'),(26,'ENTGRA-PATCH-7012','WAR','NA'),(27,'ENTGRA-PATCH-7012','RESOURCE','NA'),(28,'ENTGRA-PATCH-7005','JAR','org.wso2.carbon.device.mgt.core-3.2.1.jar'),(29,'ENTGRA-PATCH-7005','WAR','api#device-mgt#v1.0.war'),(30,'ENTGRA-PATCH-7005','RESOURCE','NA'),(31,'ENTGRA-PATCH-7011','JAR','org.wso2.carbon.device.mgt.ios.api.utils-3.4.0.jar'),(32,'ENTGRA-PATCH-7011','WAR','api#device-mgt#ios#v1.0.war'),(33,'ENTGRA-PATCH-7011','WAR','ios-enrollment.war'),(34,'ENTGRA-PATCH-7011','RESOURCE','NA'),(35,'ENTGRA-PATCH-7010','JAR','NA'),(36,'ENTGRA-PATCH-7010','WAR','NA'),(37,'ENTGRA-PATCH-7010','RESOURCE','admin--Windows_10_Enrollment_Service.xml'),(38,'ENTGRA-PATCH-7009','JAR','NA'),(39,'ENTGRA-PATCH-7009','WAR','NA'),(40,'ENTGRA-PATCH-7009','RESOURCE','h2.sql'),(41,'ENTGRA-PATCH-7009','RESOURCE','mssql.sql'),(42,'ENTGRA-PATCH-7009','RESOURCE','mysql.sql'),(43,'ENTGRA-PATCH-7009','RESOURCE','oracle.sql'),(44,'ENTGRA-PATCH-7009','RESOURCE','postgres.sql'),(45,'ENTGRA-PATCH-7008','JAR','NA'),(46,'ENTGRA-PATCH-7008','WAR','ios-enrollment.war'),(47,'ENTGRA-PATCH-7008','RESOURCE','NA'),(48,'ENTGRA-PATCH-7007','JAR','org.wso2.carbon.device.mgt.ios.api.utils-3.4.0.jar'),(49,'ENTGRA-PATCH-7007','JAR','org.wso2.carbon.device.mgt.ios.apns-3.4.0.jar'),(50,'ENTGRA-PATCH-7007','JAR','org.wso2.carbon.device.mgt.ios.core-3.4.0.jar'),(51,'ENTGRA-PATCH-7007','JAR','org.wso2.carbon.device.mgt.ios.dep-3.4.0.jar'),(52,'ENTGRA-PATCH-7007','JAR','org.wso2.carbon.device.mgt.ios.payload-3.4.0.jar'),(53,'ENTGRA-PATCH-7007','WAR','api#device-mgt#ios#v1.0.war'),(54,'ENTGRA-PATCH-7007','WAR','ios-enrollment.war'),(55,'ENTGRA-PATCH-7007','RESOURCE','agent-controller.jag'),(56,'ENTGRA-PATCH-7007','RESOURCE','agent-enroll.jag'),(57,'ENTGRA-PATCH-7007','RESOURCE','agent.jag'),(58,'ENTGRA-PATCH-7007','RESOURCE','EnrollmentPayloadGenerator.java'),(59,'ENTGRA-PATCH-7007','RESOURCE','default.js'),(60,'ENTGRA-PATCH-7007','RESOURCE','access-control.js'),(61,'ENTGRA-PATCH-7007','RESOURCE','access-control.js'),(62,'ENTGRA-PATCH-7006','JAR','org.wso2.carbon.device.mgt.common-3.2.1.jar'),(63,'ENTGRA-PATCH-7006','JAR','org.wso2.carbon.device.mgt.core-3.2.1.jar'),(64,'ENTGRA-PATCH-7006','JAR','org.wso2.carbon.device.mgt.mobile.windows-4.2.1.jar'),(65,'ENTGRA-PATCH-7006','JAR','org.wso2.carbon.policy.decision.point-3.2.1.jar'),(66,'ENTGRA-PATCH-7006','JAR',''),(67,'ENTGRA-PATCH-7006','WAR','api#certificate-mgt#v1.0.war'),(68,'ENTGRA-PATCH-7006','WAR','api#device-mgt#v1.0.war'),(69,'ENTGRA-PATCH-7006','WAR','api#device-mgt#windows#v1.0.war'),(70,'ENTGRA-PATCH-7006','RESOURCE','NA');
/*!40000 ALTER TABLE `artifact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` varchar(100) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `created_date` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES ('DIALOGNOCSUB','Dialog','2019-08-11 17:48:30.181'),('ENTGRAINTERNALSUB','Entgra internal sub','2019-08-11 17:47:08.279'),('ENTREDASUB','Entreda','2019-08-11 17:50:01.481'),('HCLSUB','HCL','2019-08-11 17:49:03.454'),('MOBICODESUB','Mobicode','2019-08-11 17:48:01.729'),('ORANGESUB','Orange','2019-08-11 17:50:17.902'),('STRYKERSUB','Stryker','2019-08-11 17:49:30.375');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patch`
--

DROP TABLE IF EXISTS `patch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patch` (
  `patch_id` varchar(20) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `issue_type` varchar(15) DEFAULT NULL,
  `customer_id` varchar(45) DEFAULT NULL,
  `public_issue` varchar(200) DEFAULT NULL,
  `private_issue` varchar(200) DEFAULT NULL,
  `reporter` varchar(45) DEFAULT NULL,
  `eta_best_case` date DEFAULT NULL,
  `eta_most_likely` date DEFAULT NULL,
  `eta_worst_case` date DEFAULT NULL,
  `developer` varchar(45) DEFAULT NULL,
  `public_pr` varchar(5000) DEFAULT NULL,
  `support_pr` varchar(5000) DEFAULT NULL,
  `current_state` varchar(45) DEFAULT NULL,
  `issue_description` varchar(5000) DEFAULT NULL,
  `created_time` bigint(11) DEFAULT NULL,
  `last_updated_time` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`patch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patch`
--

LOCK TABLES `patch` WRITE;
/*!40000 ALTER TABLE `patch` DISABLE KEYS */;
INSERT INTO `patch` VALUES ('ENTGRA-PATCH-6999',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ENTGRA-PATCH-7000',1,'BUG','ENTGRAINTERNALSUB','https://gitlab.com/entgra/product-iots/issues/11','https://gitlab.com/entgra-support/entgrainternalsub/issues/3','milan@entgra.io','2018-11-25','2018-11-26','2018-11-27','milan@entgra.io','https://gitlab.com/entgra/android-agent/merge_requests/11','https://gitlab.com/entgra/android-agent/merge_requests/11','DELIVERED','Operation fetching gets failed for iOS devices',1565546367925,1565546826132),('ENTGRA-PATCH-7001',1,'SERVICE_PACK','ENTGRAINTERNALSUB','https://gitlab.com/entgra/product-iots/issues/82','https://gitlab.com/entgra/product-iots/issues/82','milan@entgra.io','2019-04-18','2019-04-19','2019-04-20','milan@entgra.io','https://gitlab.com/entgra/product-iots/issues/82','https://gitlab.com/entgra/product-iots/issues/82','DELIVERED','Create a service pack with wso2 updates',1565547282910,1565548629110),('ENTGRA-PATCH-7002',1,'BUG','ENTGRAINTERNALSUB','https://gitlab.com/entgra/product-iots/issues/15','NA','milan@entgra.io','2019-05-06','2019-05-06','2019-05-06','milan@entgra.io','https://gitlab.com/entgra/android-agent/merge_requests/11','NA','DELIVERED','When the CHANGE-LOCK-CODE is executed on the latest android devices, it stops syncing data with the server due to a runtime issue in the agent application.',1565548730233,1565549166211),('ENTGRA-PATCH-7003',1,'IMPROVEMENT','ENTGRAINTERNALSUB','NA','NA','milan@entgra.io',NULL,NULL,NULL,NULL,NULL,NULL,'QUEUED','NA',1565549561688,1565549561688),('ENTGRA-PATCH-7004',1,'BUG','ENTGRAINTERNALSUB','NA','NA','saad@entgra.io','2019-05-31','2019-05-31','2019-05-31','saad@entgra.io',NULL,NULL,'DEVELOPMENT','NA',1565549818204,1565552294067),('ENTGRA-PATCH-7005',1,'NEW_FEATURE','MOBICODESUB','https://gitlab.com/entgra/product-iots/issues/98','https://gitlab.com/entgra-customer-support/mobicodesub/issues/13','saad@entgra.io','2019-06-13','2019-06-13','2019-06-13','saad@entgra.io','https://gitlab.com/entgra/carbon-device-mgt/merge_requests/118','https://gitlab.com/entgra-support/support-carbon-device-mgt/merge_requests/5','DELIVERED','Removed devices that have been \'removed\' from a user',1565550118723,1565551392357),('ENTGRA-PATCH-7006',1,'IMPROVEMENT','ENTGRAINTERNALSUB','Syncing 3.4.0 Support Issues','NA','saad@entgra.io','2019-06-04','2019-06-04','2019-06-04','saad@entgra.io','NA','NA','DELIVERED','Syncing 3.4.0 Support Issues',1565550219634,1565552220482),('ENTGRA-PATCH-7007',1,'IMPROVEMENT','ENTGRAINTERNALSUB','NA','NA','saad@entgra.io','2019-06-04','2019-06-04','2019-06-04','saad@entgra.io','NA','NA','DELIVERED','Patch and resources to sync 3.4.0 proprietary plugin',1565550279761,1565552155147),('ENTGRA-PATCH-7008',1,'BUG','ENTGRAINTERNALSUB','https://gitlab.com/entgra/product-iots/issues/101','https://gitlab.com/entgra/product-iots/issues/101','saad@entgra.io','2019-06-06','2019-06-06','2019-06-06','saad@entgra.io','https://gitlab.com/entgra/carbon-devicemgt-proprietary-plugins/merge_requests/35','https://gitlab.com/entgra/carbon-devicemgt-proprietary-plugins/merge_requests/35','DELIVERED','Fix iOS device payload missing values',1565550352136,1565552054432),('ENTGRA-PATCH-7009',1,'BUG','ENTGRAINTERNALSUB','https://gitlab.com/entgra/product-iots/issues/106','https://gitlab.com/entgra/product-iots/issues/101','saad@entgra.io','2019-06-11','2019-06-11','2019-06-11','saad@entgra.io','https://gitlab.com/entgra/carbon-device-mgt/merge_requests/131','https://gitlab.com/entgra/carbon-device-mgt/merge_requests/131','DELIVERED','Increase length of VALUE_FIELD column in DM_DEVICE_INFO table',1565550434053,1565551941199),('ENTGRA-PATCH-7010',1,'BUG','ENTGRAINTERNALSUB','NA','Add disable chunking property to Windows 10 enrollment synapse API','saad@entgra.io','2019-06-12','2019-06-12','2019-06-12','saad@entgra.io','NA','https://gitlab.com/entgra-support/support-carbon-device-mgt-plugins/merge_requests/10','DELIVERED','Add disable chunking property to Windows 10 enrollment synapse API',1565550491587,1565551700733),('ENTGRA-PATCH-7011',1,'IMPROVEMENT','ENTGRAINTERNALSUB','NA','External profile add and remove APIs','saad@entgra.io','2019-06-14','2019-06-14','2019-06-14','saad@entgra.io','https://gitlab.com/entgra/carbon-devicemgt-proprietary-plugins/merge_requests/38','https://gitlab.com/entgra/carbon-devicemgt-proprietary-plugins/merge_requests/38','DELIVERED','External profile add and remove APIs',1565550619454,1565551496704),('ENTGRA-PATCH-7012',1,'NEW_FEATURE','ENTGRAINTERNALSUB','https://gitlab.com/entgra/product-iots/issues/120','https://gitlab.com/entgra/product-iots/issues/120','saad@entgra.io','2019-07-27','2019-07-27','2019-07-27','saad@entgra.io','NA','https://gitlab.com/entgra/carbon-devicemgt-proprietary-plugins/merge_requests/50,https://gitlab.com/entgra/carbon-devicemgt-proprietary-plugins/merge_requests/49','DELIVERED','Install MacOS Enterprise Application along with pinning certs to download the manifest',1565550792095,1565551260785),('ENTGRA-PATCH-7013',1,'BUG','MOBICODESUB','https://gitlab.com/entgra/product-iots/issues/125','https://gitlab.com/entgra-customer-support/mobicodesub/issues/42','inosh@entgra.io','2019-08-08','2019-08-08','2019-08-08','inosh@entgra.io','https://gitlab.com/entgra/carbon-device-mgt-plugins/merge_requests/63','https://gitlab.com/entgra-support/support-carbon-device-mgt-plugins/merge_requests/15','DELIVERED','Wipe does not put the device to the removed state',1565550865520,1565551060922);
/*!40000 ALTER TABLE `patch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) DEFAULT NULL,
  `product_version` varchar(50) DEFAULT NULL,
  `carbon_version` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'entgra-iot','3.4.0','4.4.26'),(2,'entgra-iot','3.5.0','4.4.26'),(3,'entgra-iot','3.6.0','4.4.26');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-12 10:32:58
