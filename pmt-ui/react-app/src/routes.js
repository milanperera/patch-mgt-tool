/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Dashboard from "./views/Dashboard.jsx";
import PatchList from "./views/PatchList.jsx";
import Typography from "./views/Typography.jsx";
import Icons from "./views/Icons.jsx";
import Notifications from "./views/Notifications.jsx";
import PatchQueue from "./views/PatchQueue";
import PatchSearch from "./views/PatchSearch";
import PatchETA from "./views/PatchETA";
import PatchDelivery from "./views/PatchDelivery";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard,
    layout: "/pmt"
  },
  {
    path: "/patch-create",
    name: "Patch Queue",
    icon: "pe-7s-box1",
    component: PatchQueue,
    layout: "/pmt"
  },
    {
        path: "/patch-eta",
        name: "Patch ETA",
        icon: "pe-7s-box1",
        component: PatchETA,
        layout: "/pmt",
        disabled: true
    },
    {
        path: "/patch-delivery",
        name: "Patch Delivery",
        icon: "pe-7s-box1",
        component: PatchDelivery,
        layout: "/pmt",
        disabled: true
    },
  {
    path: "/patch-list",
    name: "Patch List",
    icon: "pe-7s-note2",
    component: PatchList,
    layout: "/pmt"
  },
    {
        path: "/patch-search",
        name: "Patch Search",
        icon: "pe-7s-search",
        component: PatchSearch,
        layout: "/pmt"
    },
  {
    path: "/typography",
    name: "Typography",
    icon: "pe-7s-news-paper",
    component: Typography,
    layout: "/pmt",
      disabled: true
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "pe-7s-science",
    component: Icons,
    layout: "/pmt",
      disabled: true
  },
  {
    path: "/notifications",
    name: "Notifications",
    icon: "pe-7s-bell",
    component: Notifications,
    layout: "/pmt",
      disabled: true
  }
];

export default dashboardRoutes;
