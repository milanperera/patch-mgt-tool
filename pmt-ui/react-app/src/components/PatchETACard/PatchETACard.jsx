/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
import React, {Component} from "react";
import {users} from "../../variables/Variables";
import HTTP from "../../util/HttpClient";
import {Route} from "react-router";
import {Link} from "react-router-dom";
import PatchList from "../../views/PatchList";

export class PatchETACard extends Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            developer : '',
            bcase : '',
            mlcase : '',
            wcase : ''
                   };
        this.setValues();
    };

    handleChange(event) {
        console.log(event.target.value);
        const name = event.target.name;
        this.setState({
                          [name] : event.target.value
                      });
    }

    handleSubmit() {
        const payload = {
            developer: this.state.developer,
            eta : {
                bestCase : this.state.bcase,
                mostLikely : this.state.mlcase,
                worstCase : this.state.wcase,
            }
        };
        console.log(payload);
        this.updateETA(payload);

    }

    updateETA(payload) {
        const url = 'patch/eta/' + this.props.patchId;
        console.log(url);
        HTTP.put(url, payload)
                .then( result => {
                    this.props.handleClick(result.data.message);
                    console.log(result);
                }).catch(function (error) {
            alert("Error occurred while setting the ETA. Please refer console log for more details...");
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);
        });
    }

    fetchPatch(pId) {
        let url = 'patch/' + pId;
        HTTP.get(url)
                .then(res => {
                    const result = res.data;
                    this.setState({
                                      developer : result.developer,
                                      bcase : result.eta.bestCase,
                                      mlcase : result.eta.mostLikely,
                                      wcase : result.eta.worstCase
                                  });
                });
    }

    setValues() {
        const {patchId, isSubmitButtonVisible} = this.props;
        if (patchId && !isSubmitButtonVisible) {
            this.fetchPatch(patchId);
        }
    }

    render () {
        const {submitButtonLabel, isSubmitButtonVisible, disableAll} = this.props;
        return (
                <div className="content">

                        <div className="row">
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label className="control-label">Best Case</label>
                                    <input
                                            value={this.state.bcase}
                                            type="date"
                                            name="bcase"
                                            onChange={this.handleChange}
                                            disabled={disableAll}
                                            className="form-control"/>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label className="control-label">Most Likely</label>
                                    <input
                                            value={this.state.mlcase}
                                            type="date"
                                            name="mlcase"
                                            onChange={this.handleChange}
                                            disabled={disableAll}
                                            className="form-control"/>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label className="control-label">Worst Case</label>
                                    <input
                                            value={this.state.wcase}
                                            type="date"
                                            name="wcase"
                                            onChange={this.handleChange}
                                            disabled={disableAll}
                                            className="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label className="control-label">Develop by</label>
                                    <select className="form-control" name="developer" onChange={this.handleChange} value={this.state.developer} disabled={disableAll}>
                                        <option key='0' value='0'>-- select --</option>
                                        {users.map((user, i) =>
                                                           <option key={'' + (i+1)} value={user}>{user}</option>
                                        )}
                                    </select>
                                </div>
                            </div>
                        </div>
                        {isSubmitButtonVisible && (
                        <Link
                            to="/pmt/patch-list"
                            className="btn-fill btn btn-info"
                            onClick={() => {
                                this.handleSubmit();
                            }} >
                            {submitButtonLabel}
                        </Link>
                        )}
                    <Route
                            path="/pmt/patch-list"
                            component={PatchList}
                    />
                    </div>

        );
    }
}

export default PatchETACard;