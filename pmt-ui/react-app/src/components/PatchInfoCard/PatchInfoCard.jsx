/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
import React, {Component} from "react";
import {issueTypes, users} from "../../variables/Variables.jsx";
import HTTP from "../../util/HttpClient";

export class PatchInfoCard extends Component {

    resultPayload = {};

    constructor(props) {
        super(props);
        this.state = {
            patchId : '',
            productId : '',
            issueType : '',
            client : '',
            reportedBy : '',
            publicIssue : '',
            supportIssue : '',
            issueDescription : '',
            pList : [],
            customers : []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.printError = this.printError.bind(this);
        this.getCustomerList();
        this.getPatchId();
        this.getProductList();

        this.publicIssueTextInput = React.createRef();
        this.supportIssueTextInput = React.createRef();
        this.issueDescTextInput = React.createRef();

        this.setValues();
    };


    handleChange(event) {
        const name = event.target.name;
        this.setState({
            [name] : event.target.value
        });
    }

    handleSubmit(event) {
        this.resultPayload = {
            patchId : this.state.patchId,
            productId : this.state.productId,
            issueType : this.state.issueType,
            customerId : this.state.client,
            reporter : this.state.reportedBy,
            publicIssue : this.state.publicIssue,
            privateIssue : this.state.supportIssue,
            issueDescription : this.state.issueDescription
        };
        this.addPatch(this.resultPayload);
    }

    onSuccess() {
        this.setState({
            patchId : '',
            productId : '',
            issueType : '',
            client : '',
            reportedBy : '',
            publicIssue : '',
            supportIssue : '',
            issueDescription : ''
        });
        this.getPatchId();
        this.publicIssueTextInput.current.value='';
        this.supportIssueTextInput.current.value='';
        this.issueDescTextInput.current.value='';
    }

    getPatchId() {
        HTTP.get('patch/generate-id')
            .then(res => {
                const result = res.data;
                this.setState({
                    patchId : result["patchId"]
                });
            });
    }

    getProductList() {
        HTTP.get('product/')
            .then(res => {
                const result = res.data;
                this.setState({
                    pList : result
                });
            });
    }

    getCustomerList() {
        HTTP.get('customer/')
            .then(res => {
                const result = res.data;
                this.setState({
                    customers : result
                });
            });
    }

    printError(msg) {
    }

    addPatch(patch) {
        const requestHeaders = {
            'Content-Type': 'application/json',
            'Accept' : '*/*'
        }
        HTTP.post(`patch/queue/add`, patch, {headers: requestHeaders})
            .then( result => {
                this.props.handleClick(result.data.message);
                this.onSuccess();
            }).catch(function (error) {
                alert("Error occurred while adding patch to the queue. Please refer console log for more details...");
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);
        });
    }

    fetchPatch(pId) {
        let url = 'patch/' + pId;
        HTTP.get(url)
            .then(res => {
                const result = res.data;
                this.setState({
                      patchData: result
                  });
                this.setState({
                      patchId : result.patchId,
                      productId : result.productId,
                      issueType : result.issueType,
                      client : result.customerId,
                      reportedBy : result.reporter,
                      publicIssue : result.publicIssue,
                      supportIssue : result.privateIssue,
                      issueDescription : result.issueDescription
                  });
            });
    }

    setValues() {
        const {patchId} = this.props;
        if (patchId) {
            this.fetchPatch(patchId);
        }
    }


    render() {
        const {submitButtonLabel, isSubmitButtonVisible, disableAll} = this.props;

        return (
                <div className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group">
                                <label className="control-label">Patch ID</label>
                                <input placeholder="ENTGRA-PATCH-7000"
                                       disabled={true}
                                       type="text"
                                       className="form-control"
                                       name="patchId"
                                       onChange={this.handleChange}
                                       value={this.state.patchId}/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="control-label">Product</label>
                                <select className="form-control" name="productId" onChange={this.handleChange} value={this.state.productId} disabled={disableAll}>
                                    <option className="form-control" key='0' value='0'>-- select --</option>
                                    {this.state.pList.map((product) =>
                                        <option key={'' + product.productId} value={product.productId}>{product.productName + "-" + product.productVersion}</option>
                                    )}
                                </select>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="control-label">Issue Type</label>
                                <select className="form-control" name="issueType" onChange={this.handleChange} value={this.state.issueType} disabled={disableAll}>
                                    <option key='0' value='0'>-- select --</option>
                                    {issueTypes.map((type, i) =>
                                        <option key={'' + (i+1)} value={type}>{type}</option>
                                    )}
                                </select>
                            </div>
                        </div>

                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group">
                                <label className="control-label">Client</label>
                                <select className="form-control" name="client" onChange={this.handleChange} value={this.state.client} disabled={disableAll}>
                                    <option className="form-control" key='0' value='0'>-- select --</option>
                                    {this.state.customers.map((customer, i) =>
                                        <option key={'' + (i+1)} value={customer.customerId}>{customer.customerId}</option>
                                    )}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group">
                                <label className="control-label">Reported By</label>
                                <select className="form-control" name="reportedBy" onChange={this.handleChange} value={this.state.reportedBy} disabled={disableAll}>
                                    <option key='0' value='0'>-- select --</option>
                                    {users.map((user, i) =>
                                        <option key={'' + (i+1)} value={user}>{user}</option>
                                    )}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="control-label">Public Issue</label>
                                <input placeholder="https://gitlab.com/entgra/product-iots?issue"
                                       type="text"
                                       name="publicIssue"
                                       className="form-control"
                                       onChange={this.handleChange}
                                       ref={this.publicIssueTextInput}
                                       value={this.state.publicIssue}
                                       disabled={disableAll}
                                />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="control-label">Support Issue</label>
                                <input placeholder="https://gitlab.com/entgra-customer-support/devinternal?issue"
                                       type="text"
                                       name="supportIssue"
                                       className="form-control"
                                       onChange={this.handleChange}
                                       ref={this.supportIssueTextInput}
                                       value={this.state.supportIssue}
                                       disabled={disableAll}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="formControlsTextarea" className="control-label">Issue
                                Description</label>
                                <textarea rows="5"
                                          placeholder="Summary of the issue"
                                          id="formControlsTextarea"
                                          name="issueDescription"
                                          className="form-control"
                                          onChange={this.handleChange}
                                          ref={this.issueDescTextInput}
                                          value={this.state.issueDescription}
                                          disabled={disableAll}
                                />
                            </div>
                        </div>
                    </div>
                    {isSubmitButtonVisible ?
                        (<button type="button"
                                 className="btn-fill pull-right btn btn-info"
                                 onClick={(e) => {
                                     this.handleSubmit(e);
                                 }} >
                        {submitButtonLabel}</button>)
                        : (<div/>)
                    }
                </div>
        );
    }
}

export default PatchInfoCard;
