/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

import {Component} from "react";
import {FormInputs} from "../FormInputs/FormInputs";
import React from "react";
import Button from "../CustomButton/CustomButton";

export class PatchSearchCard extends Component {

    render() {
        return (
                <div className="content">
                    <form>
                    <FormInputs
                            ncols={["col-md-2", "col-md-2", "col-md-2", "col-md-2", "col-md-2", "col-md-2"]}
                            properties={[
                                {
                                    label: "Patch ID",
                                    type: "text",
                                    bsClass: "form-control",
                                    placeholder: "ENTGRA-PATCH-7000"
                                },
                                {
                                    label: "Product Name",
                                    type: "text",
                                    bsClass: "form-control",
                                    placeholder: "Entgra-IoT"
                                },
                                {
                                    label: "Product Version",
                                    type: "text",
                                    bsClass: "form-control",
                                    placeholder: "3.4.0"
                                },
                                {
                                    label: "Customer",
                                    type: "text",
                                    bsClass: "form-control",
                                    placeholder: "DEVINTERNAL"
                                },
                                {
                                    label: "Support Issue",
                                    type: "text",
                                    bsClass: "form-control",
                                    placeholder: "https://gitlab.com/entgra"
                                },
                                {
                                    label: "State",
                                    type: "text",
                                    bsClass: "form-control",
                                    placeholder: "DELIVERED"
                                }
                            ]}
                    />
                        <Button bsStyle="info" pullRight fill type="submit">
                            Search
                        </Button>
                        <div className="clearfix" />
                    </form>
                </div>
        );
    }

}
export default PatchSearchCard;