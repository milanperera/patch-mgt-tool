/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

import {Component} from "react";
import React from "react";
import HTTP from "../../util/HttpClient";
import {Route} from "react-router";
import PatchList from "../../views/PatchList";
import {Link} from "react-router-dom";

export class PatchDeliveryCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
          publicPR : '',
          supportPR : '',
          jars : [],
          wars : [],
          resources : []
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.publicPRTextInput = React.createRef();
        this.supportPRTextInput = React.createRef();
        this.jarsTextInput = React.createRef();
        this.warsTextInput = React.createRef();
        this.resourcesTextInput = React.createRef();

        this.setValues();
    };

    handleChange(event) {
        console.log(event.target.value);
        const name = event.target.name;
        this.setState({
              [name] : event.target.value
          });
    }

    handleSubmit() {
        const payload = {
            publicPR: this.state.publicPR,
            supportPR: this.state.supportPR,
        };
        console.log(payload);
        this.promoteToQA(payload);

    }

    promoteToQA(payload) {
        console.log(this.props.patchId);
        const url = 'patch/qa/' + this.props.patchId;
        console.log(url);
        HTTP.put(url, payload)
                .then( result => {
                    this.props.handleClick(result.data.message);
                    this.promoteToDeliver()
                }).catch(function (error) {
            alert("Error occurred while promoting to QA. Please refer console log for more details...");
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);
        });
    }

    promoteToDeliver() {
        const payload = {
            jars : this.state.jars.length > 0 ? this.state.jars.split(',').map(item => item.trim()) : ["NA"],
            webapps : this.state.wars.length > 0 ? this.state.wars.split(',').map(item => item.trim()) : ["NA"],
            resources : this.state.resources.length > 0 ? this.state.resources.split(',').map(item => item.trim()) : ["NA"]
        };
        console.log(payload);
        const url = 'patch/deliver/' + this.props.patchId;
        console.log(url);
        HTTP.put(url, payload)
                .then( result => {
                    this.props.handleClick(result.data.message);
                    this.clearOnSuccess();
                }).catch(function (error) {
            alert("Error occurred while promoting to DELIVERED. Please refer console log for more details...");
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);
        });

    }

    clearOnSuccess() {
        this.publicPRTextInput.current.value='';
        this.supportPRTextInput.current.value='';
        this.jarsTextInput.current.value='';
        this.warsTextInput.current.value='';
        this.resourcesTextInput.current.value='';
    }

    setValues() {
        const {patchId, isSubmitButtonVisible} = this.props;
        if (patchId && !isSubmitButtonVisible) {
            this.fetchPatch(patchId);
        }
    }

    fetchPatch(pId) {
        let url = 'patch/' + pId;
        HTTP.get(url)
                .then(res => {
                    const result = res.data;
                    this.setState({
                          publicPR : result.publicPR,
                          supportPR : result.supportPR,
                          jars : result.patchArtifacts.jars.join(),
                          wars : result.patchArtifacts.webapps.join(),
                          resources : result.patchArtifacts.resources.join()
                                  });
                });
    }

    render() {
        const {submitButtonLabel, isSubmitButtonVisible, disableAll} = this.props;
        return (
            <div className="content">
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label className="control-label">Public PR</label>
                            <input
                                placeholder="https://gitlab.com/entgra/component"
                                type="text"
                                name="publicPR"
                                disabled={disableAll}
                                onChange={this.handleChange}
                                ref={this.publicPRTextInput}
                                value={this.state.publicPR}
                                className="form-control"/>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label className="control-label">Support PR</label>
                            <input
                                placeholder="https://gitlab.com/entgra-support/component"
                                type="text"
                                name="supportPR"
                                disabled={disableAll}
                                onChange={this.handleChange}
                                ref={this.supportPRTextInput}
                                value={this.state.supportPR}
                                className="form-control"/>
                        </div>
                    </div>
                </div>
                <h5>Patch Artifacts</h5>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label className="control-label">List of JARs (Comma Separated)</label>
                            <input
                                placeholder="org.wso2.carbon.device.mgt.core-1.2.1.jar"
                                type="text"
                                name="jars"
                                disabled={disableAll}
                                onChange={this.handleChange}
                                ref={this.jarsTextInput}
                                value={this.state.jars}
                                className="form-control"/>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label className="control-label">List of WARs (Comma Separated)</label>
                            <input
                                placeholder="api#device-mgt#v1.0.0.war"
                                type="text"
                                name="wars"
                                disabled={disableAll}
                                onChange={this.handleChange}
                                ref={this.warsTextInput}
                                value={this.state.wars}
                                className="form-control"/>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label className="control-label">List of Resources (Comma Separated)</label>
                            <input
                                placeholder="test.js"
                                type="text"
                                name="resources"
                                disabled={disableAll}
                                onChange={this.handleChange}
                                ref={this.resourcesTextInput}
                                value={this.state.resources}
                                className="form-control"/>
                        </div>
                    </div>
                </div>
                {isSubmitButtonVisible ? (
                <Link
                        to="/pmt/patch-search"
                        className="btn-fill btn btn-info pull-right"
                        onClick={this.handleSubmit}
                        >
                    {submitButtonLabel}
                </Link>
                ) : (<div/>)}
                <Route
                        path="/pmt/patch-search"
                        component={PatchList}
                />
            </div>
        );
    }
}

export default PatchDeliveryCard;