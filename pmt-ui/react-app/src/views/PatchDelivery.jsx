/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
import React, { Component } from "react";
import {
    Grid,
    Row,
    Col
} from "react-bootstrap";

import { Card } from "../components/Card/Card.jsx";
import Button from "../components/CustomButton/CustomButton.jsx";
import PatchInfoCard from "../components/PatchInfoCard/PatchInfoCard";
import PatchETACard from "../components/PatchETACard/PatchETACard";
import PatchID from "../components/PatchID/PatchID";
import PatchDeliveryCard from "../components/PatchDeliveryCard/PatchDeliveryCard";


class PatchDelivery extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {disableAll, isSubmitButtonVisible} = this.props.location.state;
        return(
                <div className="content">
                    <Grid fluid>
                        <Row>
                            <Col md={8}>
                                <Card
                                        title="Patch Info"
                                        content={
                                            <div>
                                                <PatchInfoCard
                                                        patchId={this.props.location.state ? this.props.location.state.patchId : null}
                                                        disableAll={true}
                                                />
                                            </div>
                                        }
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={8}>
                                <Card
                                        title="ETA"
                                        content={
                                            <form>
                                                <PatchETACard
                                                        patchId={this.props.location.state ? this.props.location.state.patchId : null}
                                                        disableAll={true}
                                                />
                                            </form>
                                        }
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={8}>
                                <Card
                                        title="Patch Delivery"
                                        content={
                                            <div>
                                                <PatchDeliveryCard
                                                    submitButtonLabel="Complete"
                                                    isSubmitButtonVisible={isSubmitButtonVisible}
                                                    handleClick={this.props.handleClick}
                                                    patchId={this.props.location.state ? this.props.location.state.patchId : null}
                                                    disableAll={disableAll}
                                                />
                                                <div className="clearfix" />
                                            </div>
                                        }
                                />
                            </Col>
                        </Row>
                    </Grid>
                </div>
        );
    }
}

export default PatchDelivery;
