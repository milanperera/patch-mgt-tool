/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
import React, { Component } from "react";
import {
  Grid,
  Row,
  Col
} from "react-bootstrap";

import { Card } from "../components/Card/Card.jsx";
import PatchInfoCard from "../components/PatchInfoCard/PatchInfoCard";
import NotificationSystem from "react-notification-system";
import {style} from "../variables/Variables";
import HTTP from "../util/HttpClient.jsx";

class PatchQueue extends Component {

    constructor(props) {
        super(props);
        this.state = {
            patchId: '',
            products : [],
            customers : []
        };
    };

    render() {
        return (
          <div className="content">
              <NotificationSystem ref="notificationSystem" style={style} />
            <Grid fluid>
              <Row>
                <Col md={8}>
                  <Card
                    title="Add Patch to Queue"
                    content={
                      <form>
                        <PatchInfoCard
                                submitButtonLabel="Add to Patch Queue"
                                isSubmitButtonVisible={true}
                                handleClick={this.props.handleClick}
                        />

                        <div className="clearfix" />
                      </form>
                    }
                  />
                </Col>
              </Row>
            </Grid>
          </div>
        );
    }
}

export default PatchQueue;
