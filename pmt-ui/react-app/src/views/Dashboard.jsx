/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import ChartistGraph from "react-chartist";
import { Grid, Row, Col } from "react-bootstrap";

import { Card } from "../components/Card/Card.jsx";
import PieChart from 'react-minimal-pie-chart';

import { StatsCard } from "../components/StatsCard/StatsCard.jsx";
import { Tasks } from "../components/Tasks/Tasks.jsx";
import {legendPatches} from "../variables/Variables.jsx";
import HTTP from "../util/HttpClient";

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stats: {},
            finalStats: {
                open: 0,
                dev: 0,
                qa: 0,
                com: 0
            }
        };
        this.getStats();
    }

    getStats() {
        HTTP.get('patch/stats')
                .then(res => {
                    const result = res.data;
                    this.setState({
                                      stats : result
                                  });
                    this.generateData();
                });
    }

    generateData() {
        const data = {}
        this.state.stats.map((stat, key) => {
            if (stat.lifeCycleState === 'QUEUED') {
                data["open"] = stat.count;
            }
            else if (stat.lifeCycleState === 'DEVELOPMENT') {
                data["dev"] = stat.count;
            }
            else if (stat.lifeCycleState === 'QA') {
                data["qa"] = stat.count;
            }
            else {
                data["com"] = stat.count;
            }

            if (!data.open) {
                data["open"] = 0;
            }
            if (!data.dev) {
                data["dev"] = 0;
            }
            if (!data.qa) {
                data["qa"] = 0;
            }
            if (!data.com) {
                data["com"] = 0;
            }
        });
        this.setState({
            finalStats : data
                      });
    }

  createLegend(json) {
    var legend = [];
    for (var i = 0; i < json["names"].length; i++) {
      var type = "fa fa-circle text-" + json["types"][i];
      legend.push(<i className={type} key={i} />);
      legend.push(" ");
      legend.push(json["names"][i]);
    }
    return legend;
  }
  render() {
    return (
      <div className="content">
        <Grid fluid>
          {/*<Row>*/}
            {/*<Col lg={3} sm={6}>*/}
              {/*<StatsCard*/}
                {/*bigIcon={<i className="pe-7s-server text-warning" />}*/}
                {/*statsText="Capacity"*/}
                {/*statsValue="105GB"*/}
                {/*statsIcon={<i className="fa fa-refresh" />}*/}
                {/*statsIconText="Updated now"*/}
              {/*/>*/}
            {/*</Col>*/}
            {/*<Col lg={3} sm={6}>*/}
              {/*<StatsCard*/}
                {/*bigIcon={<i className="pe-7s-wallet text-success" />}*/}
                {/*statsText="Revenue"*/}
                {/*statsValue="$1,345"*/}
                {/*statsIcon={<i className="fa fa-calendar-o" />}*/}
                {/*statsIconText="Last day"*/}
              {/*/>*/}
            {/*</Col>*/}
            {/*<Col lg={3} sm={6}>*/}
              {/*<StatsCard*/}
                {/*bigIcon={<i className="pe-7s-graph1 text-danger" />}*/}
                {/*statsText="Errors"*/}
                {/*statsValue="23"*/}
                {/*statsIcon={<i className="fa fa-clock-o" />}*/}
                {/*statsIconText="In the last hour"*/}
              {/*/>*/}
            {/*</Col>*/}
            {/*<Col lg={3} sm={6}>*/}
              {/*<StatsCard*/}
                {/*bigIcon={<i className="fa fa-twitter text-info" />}*/}
                {/*statsText="Followers"*/}
                {/*statsValue="+45"*/}
                {/*statsIcon={<i className="fa fa-refresh" />}*/}
                {/*statsIconText="Updated now"*/}
              {/*/>*/}
            {/*</Col>*/}
          {/*</Row>*/}
          <Row>
            {/*<Col md={8}>*/}
              {/*<Card*/}
                {/*statsIcon="fa fa-history"*/}
                {/*id="chartHours"*/}
                {/*title="Users Behavior"*/}
                {/*category="24 Hours performance"*/}
                {/*stats="Updated 3 minutes ago"*/}
                {/*content={*/}
                  {/*<div className="ct-chart">*/}
                    {/*<ChartistGraph*/}
                      {/*data={dataSales}*/}
                      {/*type="Line"*/}
                      {/*options={optionsSales}*/}
                      {/*responsiveOptions={responsiveSales}*/}
                    {/*/>*/}
                  {/*</div>*/}
                {/*}*/}
                {/*legend={*/}
                  {/*<div className="legend">{this.createLegend(legendSales)}</div>*/}
                {/*}*/}
              {/*/>*/}
            {/*</Col>*/}

              {console.log(this.state.finalStats)}

              <Col md={6}>
                  <Card
                          statsIcon="fa fa-clock-o"
                          title="Patch Statistics"
                          category="Current summary of patch process"
                          stats="Updated 1 minute(s) ago"
                          content={
                              <PieChart
                                      data={[
                                          { title: 'Open', value: this.state.finalStats.open, color: '#FF4A55' },
                                          { title: 'Development', value: this.state.finalStats.dev, color: '#1DC7EA' },
                                          { title: 'QA', value: this.state.finalStats.qa, color: '#FF9500' },
                                          { title: 'Delivered', value: this.state.finalStats.com, color: '#87CB16' },
                                      ]}
                                      label
                                      labelStyle={{
                                          fontSize: '5px',
                                          fontFamily: 'sans-serif',
                                      }}
                                      radius={32}
                                      labelPosition={112}
                              />
                          }
                          legend={
                          <div className="legend">{this.createLegend(legendPatches)}</div>
                          }
                  />
              </Col>
          </Row>

          {/*<Row>*/}
            {/*<Col md={6}>*/}
              {/*<Card*/}
                {/*id="chartActivity"*/}
                {/*title="2014 Sales"*/}
                {/*category="All products including Taxes"*/}
                {/*stats="Data information certified"*/}
                {/*statsIcon="fa fa-check"*/}
                {/*content={*/}
                  {/*<div className="ct-chart">*/}
                    {/*<ChartistGraph*/}
                      {/*data={dataBar}*/}
                      {/*type="Bar"*/}
                      {/*options={optionsBar}*/}
                      {/*responsiveOptions={responsiveBar}*/}
                    {/*/>*/}
                  {/*</div>*/}
                {/*}*/}
                {/*legend={*/}
                  {/*<div className="legend">{this.createLegend(legendBar)}</div>*/}
                {/*}*/}
              {/*/>*/}
            {/*</Col>*/}

            {/*<Col md={6}>*/}
              {/*<Card*/}
                {/*title="Tasks"*/}
                {/*category="Backend development"*/}
                {/*stats="Updated 3 minutes ago"*/}
                {/*statsIcon="fa fa-history"*/}
                {/*content={*/}
                  {/*<div className="table-full-width">*/}
                    {/*<table className="table">*/}
                      {/*<Tasks />*/}
                    {/*</table>*/}
                  {/*</div>*/}
                {/*}*/}
              {/*/>*/}
            {/*</Col>*/}
          {/*</Row>*/}
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
