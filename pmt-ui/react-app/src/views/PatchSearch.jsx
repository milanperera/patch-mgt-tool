/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
import React, { Component } from "react";
import {Grid, Row, Col, Table} from "react-bootstrap";
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

import Card from "../components/Card/Card.jsx";
import { patchCompleteTitleArray } from "../variables/Variables.jsx";
import PatchSearchCard from "../components/PatchSearchCard/PatchSearchCard";
import HTTP from "../util/HttpClient";
import {Link, Route} from "react-router-dom";
import PatchDelivery from "./PatchDelivery";

class PatchSearch extends Component {

    patchList = [];
    productList = [];

    constructor(props) {
        super(props);
        this.state = {
            paginatedPatchList : [],
            numberOfPages : 0,
            currentPage : 0,
            pList : [],
            pName : ''
        };
        this.getPatches(0);
        this.onPageClick = this.onPageClick.bind(this);
        this.getProductList();
    }

    getPatches(pageNo) {
        const url = "patch/deliver?pageNo=" + pageNo;
        HTTP.get(url)
                .then(res => {
                    const result = res.data;
                    this.setState({
                        paginatedPatchList : result,
                        numberOfPages : result["totalPages"]
                                  });
                    this.patchList = this.state.paginatedPatchList["results"];
                });
    }

    productName = '';
    getProductName(id) {
        if (this.state.pList) {
            this.state.pList.map((prop) => {
                if (prop["productId"] === id) {
                    this.productName = prop["productName"] + "-" + prop["productVersion"];
                    return this.productName;
                }
            });
        }
    }

    getProductList() {
        HTTP.get('product/')
                .then(res => {
                    const result = res.data;
                    this.setState({
                                      pList : result
                                  });
                });
    }

    onPageClick(e, index) {
        e.preventDefault();
        this.setState({
              currentPage: index
          });
        this.getPatches(index);
    }

  render() {
      if (this.state.paginatedPatchList["results"]) {
          this.patchList = this.state.paginatedPatchList["results"];
      }

      const { currentPage } = this.state;

    return (
      <div className="content">
        <Grid fluid>
            <Row>
                <Col md={12}>
                    <Card
                      title="Patch Queue Search"
                      category="Search patches based on available criterion"
                      ctTableFullWidth
                      ctTableResponsive
                      content={
                          <PatchSearchCard/>
                      }
                    />
                </Col>
            </Row>
          <Row>
            <Col md={12}>
              <Card
                title="Completed Patch List"
                ctTableFullWidth
                ctTableResponsive
                content={
                    <div>
                  <Table striped hover>
                    <thead>
                      <tr>
                        {patchCompleteTitleArray.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr>
                    </thead>
                    <tbody>
                    {this.patchList.map((prop, key) => {
                        return (
                                <tr key={key}>
                                    <td>{prop["patchId"]}</td>
                                    {this.getProductName(prop["productId"])}
                                    <td>{this.productName}</td>
                                    <td>{prop["issueType"]}</td>
                                    <td>{prop["customerId"]}</td>
                                    <td><a href={prop["privateIssue"]} target="_blank">{prop["privateIssue"]}</a></td>
                                    <td><a href={prop["publicIssue"]} target="_blank">{prop["publicIssue"]}</a></td>
                                    <td>
                                        <Link to={
                                            {
                                                pathname: '/pmt/patch-delivery',
                                                state: {
                                                    patchId: prop["patchId"],
                                                    isSubmitButtonVisible: false,
                                                    disableAll: true
                                                }
                                            }}
                                              className="btn-fill btn btn-primary">
                                            View
                                        </Link>
                                    </td>
                                </tr>
                        );
                    })}
                    </tbody>
                  </Table>
                        <Row>
                            <div className="col-md-6"/>
                            <div className="col-md-6">

                                    {/*{this.createPages()}*/}
                                <Pagination aria-label="Page navigation example" className="pull-right">

                                    <PaginationItem disabled={currentPage <= 0}>

                                        <PaginationLink
                                                onClick={e => this.onPageClick(e, currentPage - 1)}
                                                previous
                                                href="#"
                                        />

                                    </PaginationItem>

                                    {[...Array(this.state.numberOfPages)].map((page, i) =>
                                         <PaginationItem active={i === currentPage}
                                                         key={i}>
                                             <PaginationLink onClick={
                                                 e => this.onPageClick(e, i)}
                                                             href="#">
                                                 {i + 1}
                                             </PaginationLink>
                                         </PaginationItem>
                                    )}

                                    <PaginationItem disabled={currentPage >= this.state.numberOfPages - 1}>

                                        <PaginationLink
                                                onClick={e => this.onPageClick(e, currentPage + 1)}
                                                next
                                                href="#"
                                        />

                                    </PaginationItem>

                                </Pagination>

                            </div>
                        </Row>
                    </div>

                }

              />
            </Col>

          </Row>
        </Grid>
          <Route
                  path="/pmt/patch-delivery"
                  component={PatchDelivery}
          />
      </div>
    );
  }
}

export default PatchSearch;
