/*
 * Copyright (c) 2019, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
import React, { Component } from "react";
import {
    Grid,
    Row,
    Col
} from "react-bootstrap";

import { Card } from "../components/Card/Card.jsx";
import PatchInfoCard from "../components/PatchInfoCard/PatchInfoCard";
import PatchETACard from "../components/PatchETACard/PatchETACard";

class PatchETA extends Component {

    constructor(props) {
        super(props);
        this.state = {
            patchId: null
        }
    }


    render() {

        return (
                <div className="content">
                    <Grid fluid>
                        <Row>
                            <Col md={8}>
                                <Card
                                        title="Start Creating a Patch"
                                        content={
                                            <div>
                                                <PatchInfoCard
                                                    patchId={this.props.location.state ? this.props.location.state.patchId : null}
                                                    disableAll={true}
                                                />
                                            </div>
                                        }
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={8}>
                                <Card
                                        title="Set ETA"
                                        content={
                                            <form>
                                                <PatchETACard
                                                    submitButtonLabel="Start Development"
                                                    isSubmitButtonVisible={true}
                                                    patchId={this.props.location.state ? this.props.location.state.patchId : null}
                                                    handleClick={this.props.handleClick}
                                                />
                                                <div className="clearfix" />
                                            </form>
                                        }
                                />
                            </Col>
                        </Row>
                    </Grid>
                </div>
        );
    }
}

export default PatchETA;
