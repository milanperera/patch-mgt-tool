/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";

import Card from "../components/Card/Card.jsx";
import { patchOpenTitleArray } from "../variables/Variables.jsx";
import HTTP from "../util/HttpClient";
import {Route} from "react-router";
import {Link} from "react-router-dom"
import PatchETA from "./PatchETA";
import PatchDelivery from "./PatchDelivery";
import {Pagination, PaginationItem, PaginationLink} from "reactstrap";


class PatchList extends Component {

    patchQueue = [];
    developmentQueue = [];
    productList = [];
    productName = '';

  constructor(props) {
    super(props);
    this.getPatchQueue();
    this.getProductList();
    this.getDevelopmentQueue();
    this.state = {
        paginatedPatchList : [],
        paginatedDevelopmentList : [],
        pList : [],
        numberOfPendingPages : 0,
        numberOfDevelopmentPages : 0,
        currentPendingPage : 0,
        currentDevelopmentPage : 0
    };
  }

    getPatchQueue() {
        HTTP.get('patch/queue?pageNo=0')
            .then(res => {
                const result = res.data;
                this.setState({
                    paginatedPatchList : result,
                    numberOfPendingPages : result["totalPages"]
                });
                this.patchQueue = this.state.paginatedPatchList["results"];
            });
    }

    getDevelopmentQueue() {
        HTTP.get('patch/eta?pageNo=0')
                .then(res => {
                    const result = res.data;
                    this.setState({
                        paginatedDevelopmentList : result,
                        numberOfDevelopmentPages : result["totalPages"]
                     });
                    this.developmentQueue = this.state.paginatedDevelopmentList["results"];
                });
    }

    getProductList() {
        HTTP.get('product/')
          .then(res => {
              const result = res.data;
              this.setState({
                  pList : result
              });
          });
    }

    getProductName(id) {
      if (this.productList) {
        this.productList.map((prop, key) => {
          if (prop["productId"] === id) {
            this.productName = prop["productName"] + "-" + prop["productVersion"];
          }
        });
      }
    }


    onPageClick(e, index, status) {
        e.preventDefault();
        if (status === "pending") {
            this.setState({
                currentPendingPage: index
            });
        } else {
            this.setState({
            currentDevelopmentPage: index
          });
        }
    }


  render() {
      if (this.state.paginatedPatchList["results"]) {
          this.patchQueue = this.state.paginatedPatchList["results"];
      }
      if (this.state.pList) {
        this.productList = this.state.pList;
      }
      if (this.state.paginatedDevelopmentList["results"]) {
          this.developmentQueue = this.state.paginatedDevelopmentList["results"];
      }
      const { currentPendingPage, currentDevelopmentPage } = this.state;

    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Patch Open Queue"
                category="List of patch entries to be implemented"
                ctTableFullWidth
                ctTableResponsive
                content={
                    <div>
                  <Table striped hover>
                    <thead>
                      <tr>
                        {patchOpenTitleArray.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {this.patchQueue.map((prop, key) => {
                        return (
                          <tr key={key}>
                              <td>{prop["patchId"]}</td>
                              {this.getProductName(prop["productId"])}
                              <td>{this.productName}</td>
                              <td>{prop["issueType"]}</td>
                              <td>{prop["customerId"]}</td>
                              <td><a href={prop["privateIssue"]} target="_blank">{prop["privateIssue"]}</a></td>
                              <td>
                                  <Link to={
                                    {
                                      pathname: '/pmt/patch-eta',
                                      state: {
                                          patchId: prop["patchId"]
                                      }
                                    }}
                                      className="btn-fill btn btn-info">
                                      Start Development
                                  </Link>
                              </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                        {this.patchQueue.length == 0 ? (
                                <p className="small text-muted text-center">There is no pending patches in the queue...</p>
                        ) : (<div/>)}

                        <Row>
                            <div className="col-md-6"/>
                            <div className="col-md-6">

                                {/*{this.createPages()}*/}
                                <Pagination aria-label="Page navigation example" className="pull-right">

                                    <PaginationItem disabled={currentPendingPage <= 0}>

                                        <PaginationLink
                                                onClick={e => this.onPageClick(e, currentPendingPage - 1, "pending")}
                                                previous
                                                href="#"
                                        />

                                    </PaginationItem>

                                    {[...Array(this.state.numberOfPendingPages)].map((page, i) =>
                                      <PaginationItem active={i === currentPendingPage}
                                                      key={i}>
                                          <PaginationLink onClick={
                                              e => this.onPageClick(e, i)}
                                                          href="#">
                                              {i + 1}
                                          </PaginationLink>
                                      </PaginationItem>
                                    )}

                                    <PaginationItem disabled={currentPendingPage >= this.state.numberOfPendingPages - 1}>

                                        <PaginationLink
                                                onClick={e => this.onPageClick(e, currentPendingPage + 1)}
                                                next
                                                href="#"
                                        />

                                    </PaginationItem>
                                </Pagination>
                            </div>
                        </Row>

                    </div>

                }
              />
            </Col>

            <Col md={12}>
              <Card
                title="Patch In-Progress Queue"
                category="List of patches being implemented"
                ctTableFullWidth
                ctTableResponsive
                content={
                    <div>
                  <Table striped hover>
                    <thead>
                      <tr>
                        {patchOpenTitleArray.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr>
                    </thead>
                    <tbody>
                    {this.developmentQueue.map((prop, key) => {
                        return (
                                <tr key={key}>
                                    <td>{prop["patchId"]}</td>
                                    {this.getProductName(prop["productId"])}
                                    <td>{this.productName}</td>
                                    <td>{prop["issueType"]}</td>
                                    <td>{prop["customerId"]}</td>
                                    <td><a href={prop["privateIssue"]} target="_blank">{prop["privateIssue"]}</a></td>
                                    <td>
                                        <Link to={
                                            {
                                                pathname: '/pmt/patch-delivery',
                                                state: {
                                                    patchId: prop["patchId"],
                                                    isSubmitButtonVisible: true,
                                                    disableAll: false
                                                }
                                            }}
                                              className="btn-fill btn btn-info">
                                            Deliver Patch
                                        </Link>
                                    </td>
                                </tr>
                        );
                    })}
                    </tbody>
                  </Table>
                {this.developmentQueue.length == 0 ? (
                    <p className="small text-muted text-center">There is no pending patches in the development queue...</p>
                    ) : (<div/>)}

                        <Row>
                            <div className="col-md-6"/>
                            <div className="col-md-6">

                                {/*{this.createPages()}*/}
                                <Pagination aria-label="Page navigation example" className="pull-right">

                                    <PaginationItem disabled={currentDevelopmentPage <= 0}>

                                        <PaginationLink
                                                onClick={e => this.onPageClick(e, currentDevelopmentPage - 1, "pending")}
                                                previous
                                                href="#"
                                        />

                                    </PaginationItem>

                                    {[...Array(this.state.numberOfDevelopmentPages)].map((page, i) =>
                                         <PaginationItem active={i === currentDevelopmentPage}
                                                         key={i}>
                                             <PaginationLink onClick={
                                                 e => this.onPageClick(e, i)}
                                                             href="#">
                                                 {i + 1}
                                             </PaginationLink>
                                         </PaginationItem>
                                    )}

                                    <PaginationItem disabled={currentDevelopmentPage >= this.state.numberOfDevelopmentPages - 1}>

                                        <PaginationLink
                                                onClick={e => this.onPageClick(e, currentDevelopmentPage + 1)}
                                                next
                                                href="#"
                                        />

                                    </PaginationItem>
                                </Pagination>
                            </div>
                        </Row>

                    </div>
                }


              />
            </Col>

          </Row>
        </Grid>
          <Route
                  path="/pmt/patch-delivery"
                  component={PatchDelivery}
          />
          <Route
                  path="/pmt/patch-eta"
                  component={PatchETA}
          />
      </div>
    );
  }
}

export default PatchList;
